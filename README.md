# IOC for PBWRV vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   Tgt-PBWRV:Vac-VEG-100
    *   Tgt-PBWRV:Vac-VGP-100
    *	Tgt-PBWRV:Vac-VGP-200
    *	Tgt-PBWRV:Vac-VGP-011
    *	Tgt-PBWRV:Vac-VGP-021
    *	Tgt-PBWRV:Vac-VGP-041
    *	Tgt-PBWRV:Vac-VGP-042