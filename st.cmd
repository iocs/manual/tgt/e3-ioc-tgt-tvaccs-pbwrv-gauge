#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Tgt-PBWRV:Vac-VEG-100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VEG-100, BOARD_A_SERIAL_NUMBER = 1804110718, BOARD_B_SERIAL_NUMBER = 1902130945, BOARD_C_SERIAL_NUMBER = 1804111121, IPADDR = moxa-vac-target-pbw.cslab.esss.lu.se, PORT = 4001")

#
# Device: Tgt-PBWRV:Vac-VGP-100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VGP-100, CHANNEL = A1, CONTROLLERNAME = Tgt-PBWRV:Vac-VEG-100")

#
# Device: Tgt-PBWRV:Vac-VGP-200
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VGP-200, CHANNEL = A2, CONTROLLERNAME = Tgt-PBWRV:Vac-VEG-100")

#
# Device: Tgt-PBWRV:Vac-VGP-011
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VGP-011, CHANNEL = B1, CONTROLLERNAME = Tgt-PBWRV:Vac-VEG-100")

#
# Device: Tgt-PBWRV:Vac-VGP-021
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VGP-021, CHANNEL = B2, CONTROLLERNAME = Tgt-PBWRV:Vac-VEG-100")

#
# Device: Tgt-PBWRV:Vac-VGP-041
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VGP-041, CHANNEL = C1, CONTROLLERNAME = Tgt-PBWRV:Vac-VEG-100")

#
# Device: Tgt-PBWRV:Vac-VGP-042
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-PBWRV:Vac-VGP-042, CHANNEL = C2, CONTROLLERNAME = Tgt-PBWRV:Vac-VEG-100")


